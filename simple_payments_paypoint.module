<?php

/**
 * Implement hook_menu() .
 */
function simple_payments_paypoint_menu() {

  $items['system/simple_payments_paypoint/ipn'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'simple_payments_paypoint_ipn',
    'access callback' => TRUE
  );

//   $items['admin/settings/simple-payments/paypoint'] = array(
//     'title' => 'PayPoint',
//     'page callback' => 'drupal_get_form',
//     'page arguments' => array('simple_payments_paypoint_admin_form'),
//     'description' => 'Administer PayPoint.',
//     'access arguments' => array('administer simple payments'),
//   );

  return $items;
}
/**
 * Creates a PayPoint payment form.
 *
 * A button will still need to be added to the form before it can be used.
 *
 * @param $vars
 *   An array of variables to be passed to PayPoint.
 *   They can be PayPoint variables from
 *     https://www.paypoint.com/IntegrationCenter/ic_std-variable-ref-buy-now.html
 *   and the following:
 *     'uid' - the uid of the user who is making this payment (defaults to the current user)
 *     'nid' - the nid of the node this payment relates to
 *     'module' - the module that should receive a callback when the payment is complete
 *     'type' - any payment subclassification the module wishes to use
 *     'custom' - data specific to the module / type, e.g. cart_id
 *
 * @return
 *   A generated FAPI form
 *
 * @see simple_payments_moneybookers_payment_form()
 */
function simple_payments_paypoint_payment_form($vars = array()) {

// 	$vars['cmd'] = '_xclick';
	$vars['callback'] = url('system/simple_payments_paypoint/ipn',
    array('absolute' => TRUE));
  ///TODO instead of using the lazy route use the custom field similar to the
  ///     paypal implementation - so there's one interface for webform_pay for sid
  $vars['cb_flds'] = 'merchant:trans_id:currency:module:type:nid:sid:uid:custom:urls';
  $vars['md_flds'] = 'trans_id:amount:callback:module:type:sid:urls';
  $vars['cb_post'] = 'true';
//   $vars['digest'] = md5($vars['trans_id'] . $vars['amount'] . $vars['callback']);
  $vars['amount'] = number_format($vars['amount'], 2, '.', '');

  $digest = $vars['trans_id'] . $vars['amount'] . $vars['password'];
//   dsm($digest);
  $vars['digest'] = md5(str_replace(" ", "_", $digest));
  $url = $vars['url'];
  // little hackish
  if ($vars['merchant'] && $vars['digestkey']) {
    $settings = variable_get('simple_payments_paypoint_settings', array());
    if (!is_array($settings['merchants'])) {
      $settings['merchants'] = array();
    }
    if (!is_array($settings['transactions'])) {
      $settings['transactions'] = array();
    }
    $settings['merchants'][$vars['merchant']]['digestkey'] = $vars['digestkey'];
    variable_set('simple_payments_paypoint_settings', $settings);
  }
  $protected = array("password", "url", "digestkey");
  foreach($protected as $key) {
    unset($vars[$key]);
  }
  /// paypoint doesn't handle base64 encoding well
  $keepers = array("module", "type", "nid", "uid");
  $oldvars = $vars;
  $form = simple_payments_build_form($vars);
  foreach($keepers as $key) {
    $form[$key] = array("#value" => $oldvars[$key], "#type" => 'hidden');
  }
//   dsm($form);
  $form['#action'] = $url;
//   $form['#method'] = 'GET';
	return $form;
}
function _simple_payments_paypoint_authentication($parameters) {
  $settings = variable_get('simple_payments_paypoint_settings', array());
  $format = 'trans_id:amount:callback:module:type:urls';
  $fields = explode(':', $format);
  foreach ($fields as $field) {
    $args[] = $field .'='. $parameters[$field];
  }
  if ($parameters['merchant']) {
    if ($settings['merchants'][$parameters['merchant']]['digestkey']) {
      $digestkey = $settings['merchants'][$parameters['merchant']]['digestkey'];
    }
  }
  $args[] = $digestkey;
  $unhashed = implode('&', $args);
  $hashed = md5($unhashed);
  $match = ($parameters['hash'] === $hashed) ? TRUE : FALSE;
//   watchdog("PayPoint", t("got digestkey @digestkey, @unhashed",
//     array("@digestkey" => $digestkey, "@unhashed" => $unhashed)));
  return array('hash' => $hashed, 'match' => $match);
}


/**
 * Handles an incoming PayPoint IPN.
 */
function simple_payments_paypoint_ipn() {
	$ipn = $_POST;

  watchdog("PayPoint", t("got ipn <pre>@request</pre>", array(
    "@request" => print_r($ipn, 1))));
  $status = _simple_payments_paypoint_ipn_verify($ipn, $parameters);
	if($parameters['fatal']) {
//     drupal_set_message($parameters['comment']);
//     drupal_access_denied();
    if ($parameters['urls']) {
      list($success,$error) = explode(",", $parameters['urls']);
      echo simple_payments_paypoint_read_url(trim($error));
      exit;
    }
    echo theme("simple_payments_paypoint_page", $parameters);
    exit;
  }
  $ipn['payment_status'] = ($status ? 'Completed' : 'Incomplete');
//   $payment = simple_payments_explode_custom($ipn['custom']);
  $payment['module'] = check_plain($ipn['module']);
  $payment['type'] = check_plain($ipn['type']);
  $payment['nid'] = check_plain($ipn['nid']);
  $payment['uid'] = check_plain($ipn['uid']);

  $payment['gateway'] = 'paypoint';
	$payment['currency'] = $ipn['currency'];
	$payment['amount'] = bcmul($ipn['amount'], 100);
  $payment['timestamp'] = $_SERVER['REQUEST_TIME'];
	$payment['details'] = $ipn;

	simple_payments_payment_received($payment);
  if ($parameters['urls']) {
    list($success, $error) = explode(",", $parameters['urls']);
    echo simple_payments_paypoint_read_url(trim($success));
  }
  echo theme("simple_payments_paypoint_page", $parameters);
}
function _simple_payments_paypoint_ipn_verify($ipn, &$parameters) {
  // Construct a new array to hold all our cleaned data as the number of values in POST can vary
  foreach ($ipn as $name => $value) {
    $parameters[check_plain($name)] = check_plain($value);
  }
  // Add in some of our own parameters.
  $parameters['referrer'] = request_uri();
  $parameters['callback'] = url('system/simple_payments_paypoint/ipn',
    array('absolute' => TRUE));;

  $authentication = _simple_payments_paypoint_authentication($parameters);
  $parameters['authentication_match'] = ($authentication['match']) ? 'true' : 'false';
  $parameters['hashed'] = $authentication['hash'];
  $parameters['message'] = _simple_payments_paypoint_authorisation_code($parameters);
  if (!$parameters['trans_id']) {
    $parameters['fatal'] = true;
    $parameters['comment'] = t('We were unable to process your payment. Please verify your details and try again. If the problem persists, please contact us. Many thanks. <a href="mailto:!email">!email</a>', array('!email' => variable_get('uc_store_email', '')));
    watchdog("PayPoint", t("No trans_id: <pre>@request</pre>", array(
      "@request" => print_r($parameters, 1))));
    return false;
  }

  //$output .= '<pre>'. print_r($order, true) .'</pre>';
  //$output .= '<pre>'. print_r($authentication, true) .'</pre>';
  //$output .= '<pre>'. print_r($_SESSION, true) .'</pre>';

  // Sometimes when the authentication doesn't match the payment is still taken
  // so we just log an admin comment to the order when this occurs to avoid
  // confusing users.
  if ($parameters['authentication_match'] == 'false') {
    $comment = t('Authentication of hash failed for this transaction: @params',
      array('@params' => print_r($parameters, 1)));
    watchdog("PayPoint", $comment);
  }

  switch ($parameters['code']) {

    case 'A': // Transaction authorised.
      $comment = t('Payment successfully processed by PayPoint for transaction #@transaction.', array('@transaction' => $parameters['trans_id']));
      break;

    case 'N': // Payment not authorised by the bank.
      $output .= t('The transaction was not authorised by the bank. Please contact us to complete your order <a href="mailto:!email">!email</a>', array('!email' => variable_get('uc_store_email', '')));
//       $response_code = _uc_paypoint_response_code($parameters['resp_code']);
      $message = $parameters['message'] . $response_code;
      break;

    case "C": // Communication error.
    default:
      $output .= t('A communication error occurred. Please try again later or contact us to complete your order <a href="mailto:!email">!email</a>', array('!email' => variable_get('uc_store_email', '')));
      $message = t('Communication error');
      break;

    case "P:A": case "P:X": case "P:P": case "P:S": case "P:E": case "P:I":
    case "P:C": case "P:T": case "P:N": case "P:M": case "P:B": case "P:D":
    case "P:V": case "P:R": case "P:#": // Pre-bank checks failed.
      $output .= t('The details of the order are not sufficient to complete the transaction and we were unable to process your payment. Please verify your details and try again. If the problem persists, please contact us. Many thanks. <a href="mailto:!email">!email</a>', array('!email' => variable_get('uc_store_email', '')));
  }
  //This makes sense - we can only hope PayPoint deals with other problems
  $parameters['fatal'] = ($parameters['code'] !== 'A');

  $message .= _simple_payments_paypoint_authorisation_code($parameters);
  watchdog('PayPoint', $message, WATCHDOG_NOTICE);

//   if (isset($parameters['cv2avs'])) {
//     $cv2avs_message = t('CV2AVS message: ') . _uc_paypoint_cv2avs_message($parameters['cv2avs']);
//     uc_order_comment_save($order->order_id, 0, $cv2avs_message, 'admin');
//   }

//   print uc_paypoint_debug($parameters, $order);
//   print $output;
//   drupal_set_message($output);
//   drupal_goto('<front>');
  $parameters['comment'] = $comment;
  $parameters['message'] = $message;
  $parameters['output'] = $output;
  return ($parameters['code'] == 'A');
}
function _simple_payments_paypoint_authorisation_code($parameters) {
  switch ($parameters['code']) {
    case "A":
      return t('Transaction authorised by bank. Authorisation code !auth_code available as bank reference.', array('!auth_code' => $parameters['auth_code']));
    case "N":
      return t('Transaction not authorised.');
    case "C":
      return t('Communication problem. Trying again later may well work');
    case "P:A":
      return t('Pre-bank checks. Amount not supplied or invalid.');
    case "P:X":
      return t('Pre-bank checks. Not all mandatory parameters supplied.');
    case "P:P":
      return t('Pre-bank checks. Same payment presented twice.');
    case "P:S":
      return t('Pre-bank checks. Start date invalid.');
    case "P:E":
      return t('Pre-bank checks. Expiry date invalid.');
    case "P:I":
      return t('Pre-bank checks. Issue number invalid.');
    case "P:C":
      return t('Pre-bank checks. Card number fails LUHN check.');
    case "P:T":
      return t('Pre-bank checks. Card type invalid - i.e. does not match card number prefix.');
    case "P:N":
      return t('Pre-bank checks. Customer name not supplied.');
    case "P:M":
      return t('Pre-bank checks. Merchant does not exist or not registered yet.');
    case "P:B":
      return t('Pre-bank checks. Merchant account for card type does not exist.');
    case "P:D":
      return t('Pre-bank checks. Merchant account for this currency does not exist.');
    case "P:V":
      return t('Pre-bank checks. CV2 security code mandatory and not supplied / invalid.');
    case "P:R":
      return t('Pre-bank checks. Transaction timed out awaiting a virtual circuit. Merchant may not have enough virtual circuits for the volume of business..');
    case "P:#":
      return t('Pre-bank checks. No MD5 hash / token key set up against account.');
  }
}
function simple_payments_paypoint_read_url($url) {

  $ch = curl_init($url);

  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  ob_start();
  if (curl_exec($ch)) {
    $info = ob_get_contents();
    curl_close($ch);
    ob_end_clean();
  }
  return $info;
}

function theme_simple_payments_paypoint_page($parameters) {
  $ret = theme("page",
    "<div>$message</div>
     <div>$comment</div>
     <div>$output</div>");
  return $ret;
}